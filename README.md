Forklift truck hire, sales & repairs in Leicestershire, Nottinghamshire, Birmingham, and Northampton. Short or long term contracts available.

Our team of fully-trained service engineers and a custom built, fully stocked workshops in Hinckley and Ilkeston are all in place so that in the case of a breakdown, the absolute minimum disruption to your productivity is guaranteed.

Our Service Engineers are located across the country, there's always someone close to hand in an emergency. You can trust Angus to be there when you need us.

Angus Lift Trucks is an accredited member of the Fork Lift Truck Association (FLTA).

Website: https://www.anguslifttrucks.co.uk/
